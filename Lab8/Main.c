#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include"Vector.h"
#include"Matrix.h"
 
int main()
{
	pthread_t* thread;  
	int i, j;
	int a;
	printf("MATRIX x MATRIX MULTIPLICATION\n");
	initM(m1);
	initM(m2);
	thread = (pthread_t*) malloc(num*sizeof(pthread_t));
	 
	  
	for (i = 0; i < num; i++)
	{

		if (pthread_create (&thread[i], NULL, multMatrix, (void*)i) != 0 )
		{
			perror("Can't create thread");
			free(thread);
			exit(-1);
		}
		 
	}

	for (i = 0; i < num; i++)
		pthread_join (thread[i], NULL);
	 
	free(thread);
	printf("Product:\n");
	for(i=0; i<SIZE; i++)
	{
		for(j=0; j<SIZE; j++)
		{
			printf("%d    ",result[i][j]);
		}
		printf("\n");
	}
	printf("\n\nMATRIX x VECTOR MULTIPLICATION\n");
	initM(m1);
	initV(v1);
	thread = (pthread_t*) malloc(num*sizeof(pthread_t));
	 
	  
	for (i = 0; i < num; i++)
	{

		if (pthread_create (&thread[i], NULL, multVector, (void*)i) != 0 )
		{
			perror("Can't create thread");
			free(thread);
			exit(-1);
		}
		 

	}

	for (i = 0; i < num; i++)
		pthread_join (thread[i], NULL);
	 
	free(thread);
	printf("\nProduct:\n");
	for(i=0; i<SIZE; i++)
	{
		printf("%d    ",product[i]);
	}
	printf("\n");

	
	 
	return 0;
 
}
