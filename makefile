matrix : Main.o Vector.o Matrix.o multM.o multV.o
	gcc -o matrix Main.o Vector.o Matrix.o multM.o multV.o -pthread
Main.o : Main.c 
	gcc -c Main.c -pthread

Vector.o : Vector.c Vector.h
	gcc -c Vector.c -pthread

Matrix.o : Matrix.c Matrix.h
	gcc -c Matrix.c -pthread

multM.o : multM.c Matrix.h
	gcc -c multM.c -pthread

multV.o : multV.c Matrix.h Vector.h
	gcc -c multV.c -pthread

     clean :
	rm Matrix.o Vector.o Main.o